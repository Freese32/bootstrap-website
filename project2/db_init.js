var mongoose = require('mongoose');
var User = require('./models/users');

// connect to mongodb (make sure it is running)
// this will create the bootstrapWebsiteDB database if it doesn't exist
var dbOptions = {};
mongoose.connect('mongodb://localhost/bootstrapWebsiteDB', dbOptions, function(err) {
  if (err) console.error('Error connecting to mongodb: ' + err.message);
  else console.log('Connected to mongodb!');
})

var db_init = function() {
	// create admin user if it doesn't exist
	User.findOne({username: 'freese32'}, function(err, user) {
		if (err) console.error('Error setting up admin user: ' + err.message);
		
		else if(user == null) {
		var mysort = { name: 1 };
			// create admin user
			var adminUser = new User({
			  name: 'Jaron Magana',
			  username: 'freese3207',
			  password: '1234freese32!',
			  admin: true,
			  location: 'Georgebama (A.K.A. Columbus), United States',
			  email: 'jaronmagana@yahoo.com',
              age: 'your age is 18'
});
 

			// call the built-in save method to save to the database
			adminUser.save(function(err) {
			  if (err) console.error('Error saving up admin user: ' + err.message);
			  else console.log('Admin User saved successfully!');
			});
		}
		else {
			console.log('Admin User already exists');
		}
		

	});

}

module.exports = db_init;
