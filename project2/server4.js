// server.js
// load the things we need
var express = require('express');
var app = express();
var router = express.Router();
var ejs = require('ejs');
var User = require('./models/users');
var bodyParser = require('body-parser');


// init the db
var db_init = require('./db_init');
db_init();

app.use(bodyParser.urlencoded({ extended: false }))

app.post("/signup", function(req,res,next) {
    console.log("Enter signup");	
    console.log(req.body);
	User.findOne({username:req.body.username }, function(err, user) {
		if (err) console.error('Error find user: ' + err.message);
		else if(user == null) {
		var mysort = { name: 1 };
			// create admin user
			var adminUser = new User({
			  name: req.body.firstname + " " + req.body.lastname,
			  username: req.body.username,
			  password: req.body.password,
			  email: req.body.email,
			  admin: false,
			  location: req.body.state + ", " + req.body.country,
			});

			// call the built-in save method to save to the database
			adminUser.save(function(err) {
			  if (err) console.error('Error saving up admin user: ' + err.message);
			  else console.log('User saved successfully!');
			  res.redirect(307, '/');
			});
		}
		else {
			console.log('Admin User already exists');
            res.redirect(307, '/');
		}

	});
});

app.post("/login", function(req,res,next) {
    console.log("Login");	
    console.log(req.body);
	User.findOne({username: req.body.username}, function(err, user) {
	if (err) console.error('Error find user: ' + err.message);
		else if(user == null) {
	var adminUser = User({
		name: req.body.firstname + " " + req.body.lastname,
		username: req.body.username,
		password: req.body.password,
	});
	}
  });
});
	

// set the view engine to ejs
app.set('view engine', 'ejs');

// use res.render to load up an ejs view file

// index page 
app.get('/', function(req,res,next) {
    res.render('website/homeboot',{
    page_name:'Home'

  });
});

app.get('/contact', function(req,res,next) {
    res.render('website/contact',{
    page_name:'Contact Us'

  });
});


app.get('/aug', function(req,res,next) {
    res.render('website/aug',{
    page_name:'About Us'

 });
});

app.get('/game', function(req,res,next) {
    res.render('website/game',{
    page_name:'game'

 });
});

app.get('/users', function(req, res, next) {
    User.find({}, function(err, users) {
        if(err) {
            throw err;
        }

        res.render('website/users', {
            page_name: 'Users',
            users: users //this is all users in the db
        });
    });
});

app.get('/drawboard', function(req,res,next) {
    res.render('website/drawboard',{
    page_name:'drawboard'

 });
});

app.get('/terms', function(req,res,next) {
    res.render('website/terms',{
    page_name:'terms'

 });
});

app.get('/signup2', function(req,res,next) {
    res.render('website/signup2',{
    page_name:'signup2'

 });
  });
  
 app.get('/blog', function(req,res,next) {
    res.render('website/blog',{
    page_name:'Blog'

 });
});

 app.get('/Videos', function(req,res,next) {
    res.render('website/Presentation_videos',{
    page_name:'Videos'

 });
});

app.listen(3000);
console.log('3000 is the non-magic port');


